

import fhir

p1 = fhir.Patient()
assert(not p1.initialized());

p2 = fhir.Patient("aabb0-cc4e-9c8c")  # GET API
assert(p2.initialized());

p3 = fhir.Patient("xyz") # GET API failed.
assert(not p3.initialized());

p1.get_from("ddbb0-cc4e-9c8c");
assert(p1.initialized());

p2.erase();  # DELETE API.
assert(not p2.initialized());

p2.data.abc.cde += "__AA";
p2.update();  # PUT API.

p1.create_default();
assert(p1.initialized());
p1.data.abc.cde += "__A";
p1.post()  # POST Api with default value.



search = fhir.Search();


p_list = search.search(family="aab");



