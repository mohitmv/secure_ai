#include <iostream>
#include "httplib.h"

#include "fhir.hpp"

using std::string;
using std::cout;
using std::endl;


namespace {

py::dict ReturnPatient(const std::shared_ptr<httplib::Response>& response) {
  if (response) {
    if (response->status / 100 == 2) {
      return py::module::import("json").attr("loads")(response->body);
    }
    throw std::runtime_error(string("Error: ") +
                             std::to_string(response->status) +
                             "\n" + response->body);
  }
  throw std::runtime_error("Internal Error");
}

void DatalessResponse(const std::shared_ptr<httplib::Response>& response) {
  if (not response) {
    throw std::runtime_error("Internal Error");
  }
  if (response->status / 100 != 2) {
    throw std::runtime_error(string("Error: ") +
                             std::to_string(response->status));
  }
}

FhirResponse MakeFhirResponse(
    const std::shared_ptr<httplib::Response>& response) {
  FhirResponse output;
  if (response) {
    output.status = response->status;
    output.body = response->body;
  } else {
    output.status = 500;
  }
  return output;
}

}  // namespace



string FhirResponse::DebugString() const {
  if(status/100 == 2) {
    return body;
  }
  return string("Error: ") + std::to_string(status) + "\n" + body;
}

FhirClient::FhirClient(): client("stu3.test.pyrohealth.net") {
  json_dump = py::module::import("json").attr("dumps");
}

py::dict FhirClient::GetPatient(const string& id) {
  string path = string("/fhir/Patient/") + id;
  return ReturnPatient(client.Get(path.c_str(), headers));
}

py::dict FhirClient::PutPatient(const std::string& id, const py::dict& data) {
  string path = string("/fhir/Patient/") + id;
  return ReturnPatient(client.Put(path.c_str(),
                                  headers,
                                  json_dump(data).cast<string>(),
                                  "application/fhir+json"));
}

py::dict FhirClient::PostPatient(const py::dict& data) {
  string path = "/fhir/Patient";
  return ReturnPatient(client.Post(path.c_str(),
                                   headers,
                                   json_dump(data).cast<string>(),
                                   "application/fhir+json"));
}

void FhirClient::DeletePatient(const string& id) {
  string path = string("/fhir/Patient/") + id;
  DatalessResponse(client.Delete(path.c_str()));
}


FhirResponse FhirClient::GetRequest(const string& path) {
  return MakeFhirResponse(client.Get(path.c_str(), headers));
}

FhirResponse FhirClient::PutRequest(const std::string& path, const py::dict& data) {
  return MakeFhirResponse(client.Put(path.c_str(),
                                     headers,
                                     json_dump(data).cast<string>(),
                                     "application/fhir+json"));
}

FhirResponse FhirClient::PostRequest(const string& path, const py::dict& data) {
  return MakeFhirResponse(client.Post(path.c_str(),
                                     headers,
                                     json_dump(data).cast<string>(),
                                     "application/fhir+json"));
}

FhirResponse FhirClient::DeleteRequest(const string& path) {
  return MakeFhirResponse(client.Delete(path.c_str()));
}


