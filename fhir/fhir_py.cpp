#include <pybind11/pybind11.h>
#include "fhir.hpp"

namespace py = pybind11;
using std::string;

string Ping(int x) {
    return string("pong-") + std::to_string(x+1);
}


PYBIND11_MODULE(fhir, m) {
    m.doc() = "fhir plugin"; // optional module docstring

    m.def("Ping", &Ping, "A function for testing");

    py::class_<FhirResponse>(m, "FhirResponse")
          .def_readonly("status", &FhirResponse::status)
          .def_readonly("body", &FhirResponse::body)
          .def("__repr__", &FhirResponse::DebugString);

    py::class_<FhirClient>(m, "FhirClient")
          .def(py::init<>())
          .def("GetPatient", &FhirClient::GetPatient)
          .def("PutPatient", &FhirClient::PutPatient)
          .def("PostPatient", &FhirClient::PostPatient)
          .def("DeletePatient", &FhirClient::DeletePatient)
          .def("GetRequest", &FhirClient::GetRequest)
          .def("PutRequest", &FhirClient::PutRequest)
          .def("PostRequest", &FhirClient::PostRequest)
          .def("DeleteRequest", &FhirClient::DeleteRequest);
}

