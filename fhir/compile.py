#!/usr/bin/env python3

# This is a simple incremental build system.

import os, hashlib, json
from distutils.sysconfig import get_python_inc


CXX = "g++"
PYTHON3_INCLUDE_PATH = None
CCFLAGS = ""
LINKFLAGS = ""

def ReadFile(file):
  with open(file) as fd:
    return fd.read();

def WriteFile(file, content):
  with open(file, "w") as fd:
    return fd.write(content);


class CacheStore:
  cache_file = "/tmp/fhir_cache.json"
  def __init__(self):
    if os.path.exists(self.cache_file):
      self.data = json.loads(ReadFile(self.cache_file))
    else:
      self.data = {};

  def Update(self, file, value):
    self.data[file] = value;
    WriteFile(self.cache_file, json.dumps(self.data));

  def Get(self, file):
    if file in self.data:
      return self.data[file]
    return None

cache_store = CacheStore();

def RunLinuxCommand(c):
  print("Running: " + c);
  error_code = (os.system(c) >> 8);
  if (error_code != 0):
    print("["+str(error_code)+"]Error Exit !");
    exit(error_code);


def Compile(file, output, flags=""):
  RunLinuxCommand(CXX + " " + CCFLAGS + " " + flags + " -c -std=c++11 -fPIC " +
                  file + " -o " + output);


def GetChecksum(file):
  return hashlib.md5(ReadFile(file).encode("utf-8")).hexdigest();


def CompileIfRequired(file, output, flags=""):
  if cache_store.Get(file) != GetChecksum(file):
    Compile(file, output, flags);
    cache_store.Update(file, GetChecksum(file))


def CompileMain():
  dependency_objects = " ".join(["build/httplib.o", "fhir_py.cpp",
                                 "build/fhir.o"]);
  command = CXX + " " + CCFLAGS + " -O3 -Wall -shared -std=c++11 -fPIC " + \
              PYTHON3_INCLUDE_PATH +                                       \
              " -I../toolchain/pybind11/include " +                        \
              " -I../toolchain/httplib " +                                 \
              dependency_objects +                                         \
              " -o build/fhir`python3-config --extension-suffix` " +       \
              LINKFLAGS;
  RunLinuxCommand(command);


def BasicTest():
  import build.fhir as fhir;
  assert(fhir.Ping(33) == "pong-34");
  print("BasicTest Passed");


def InitFlags():
  global CXX, PYTHON3_INCLUDE_PATH, LINKFLAGS
  PYTHON3_INCLUDE_PATH = "-I" + get_python_inc();
  if os.environ.get("MACHINE") == "laptop":  # MacOS
    CXX = "g++"
    LINKFLAGS = "-undefined dynamic_lookup";
  elif os.environ.get("MACHINE") == "workplace":
    CXX = "/usr/local/scaligent/toolchain/crosstool/v4/x86_64-unknown-linux-gnu/bin/x86_64-unknown-linux-gnu-g++"
    LINKFLAGS = "-static-libstdc++"


def BuildPythonLibrary():
  CompileIfRequired("../toolchain/httplib/httplib.cpp", "build/httplib.o");
  CompileIfRequired("fhir.cpp", "build/fhir.o",
                    "-I../toolchain/httplib -I../toolchain/pybind11/include " +
                      PYTHON3_INCLUDE_PATH);
  CompileMain();
  BasicTest();


def BuildAndRunTest():
  CompileIfRequired("../toolchain/httplib/httplib.cpp", "build/httplib.o");
  CompileIfRequired("fhir.cpp", "build/fhir.o", "-I../toolchain/httplib");
  RunLinuxCommand(CXX + " -std=c++11 test.cpp build/fhir.o build/httplib.o -I../toolchain/httplib -o build/test");
  RunLinuxCommand("./build/test");


InitFlags();
RunLinuxCommand("mkdir -p build");


if True:
  BuildPythonLibrary();
else:
  BuildAndRunTest();


