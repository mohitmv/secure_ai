from build.fhir import FhirClient, Ping
import unittest, json

assert(Ping(33) == "pong-34");

fhir = FhirClient();

class TestPatientCRUD(unittest.TestCase):
  def test_patient_apis(self):
    patient_id = "45cc4812-0718-4abb-98d0-4c4179c7575a";
    patient = fhir.GetPatient(patient_id);
    patient2 = dict(patient);
    patient2.pop("id")
    patient2["address"][0]["city"] += "__q"
    patient2_response = fhir.PostPatient(patient2);
    patient2_id = patient2_response["id"];
    patient2_get = fhir.GetPatient(patient2_id);
    patient2_get.pop("id");
    patient2_get.pop("meta");
    patient2.pop("meta");
    self.assertEqual(patient2, patient2_get);
    patient2_response["address"][0]["city"] += "__aa"
    fhir.PutPatient(patient2_id, patient2_response);
    patient2_get = fhir.GetPatient(patient2_id);
    self.assertEqual(patient2_response["address"][0]["city"],
                     "Brisbane__q__aa");
    fhir.DeletePatient(patient2_id);
    print("Deleted patient: " + patient2_id)
    self.assertRaises(RuntimeError, lambda: fhir.GetPatient(patient2_id))


class TestGeneralPurposeAPIs(unittest.TestCase):
  def test_general_purpose_apis(self):
    patient_id = "45cc4812-0718-4abb-98d0-4c4179c7575a"
    patient = fhir.GetRequest("/fhir/Patient/" + patient_id)
    self.assertEqual(2, patient.status // 100)
    # General purpose fhir APIs returns an object of type FhirRespons.
    # It has two fields - (1). status : int (2). body : string.
    patient2 = json.loads(patient.body)
    patient2.pop("id")
    patient2["address"][0]["city"] += "__q"
    patient2_response = fhir.PostRequest("/fhir/Patient", patient2)
    self.assertEqual(2, patient2_response.status // 100)
    patient2_response = json.loads(patient2_response.body)
    patient2_id = patient2_response["id"]
    patient2_get = fhir.GetRequest("/fhir/Patient/" + patient2_id)
    self.assertEqual(2, patient2_get.status // 100)
    patient2_get = json.loads(patient2_get.body)
    patient2_get.pop("id")
    patient2_get.pop("meta")
    patient2.pop("meta")
    self.assertEqual(patient2, patient2_get)
    patient2_response["address"][0]["city"] += "__aa"
    resp = fhir.PutRequest("/fhir/Patient/" + patient2_id, patient2_response)
    self.assertEqual(2, resp.status // 100)
    patient2_get = fhir.GetRequest("/fhir/Patient/" + patient2_id)
    self.assertEqual(2, patient2_get.status // 100)
    patient2_get = json.loads(patient2_get.body)
    self.assertEqual(patient2_response["address"][0]["city"],
                     "Brisbane__q__aa");
    fhir.DeleteRequest("/fhir/Patient/" + patient2_id)
    print("Deleted patient: " + patient2_id)
    self.assertEqual(410, fhir.GetRequest("/fhir/Patient/" + patient2_id).status)


if __name__ == '__main__':
    unittest.main()
