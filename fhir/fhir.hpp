#ifndef _FHIR_HPP_
#define _FHIR_HPP_

#include <string>
#include <pybind11/pybind11.h>
#include "httplib.h"

namespace py = pybind11;

struct FhirResponse {
  int status;
  std::string body;
  std::string DebugString() const;
};


// Main class.
class FhirClient {
 public:
  FhirClient();

  // General purpose REST clients.
  FhirResponse GetRequest(const std::string& path);
  FhirResponse PutRequest(const std::string& path, const py::dict& data);
  FhirResponse PostRequest(const std::string& path, const py::dict& data);
  FhirResponse DeleteRequest(const std::string& path);

  // REST clients for patients.
  // throws exception in case of erroneous response.
  py::dict GetPatient(const std::string& id);
  py::dict PutPatient(const std::string& id, const py::dict& data);
  py::dict PostPatient(const py::dict& data);
  void DeletePatient(const std::string& id);

 private:
  py::object json_dump;
  httplib::Client client;
  httplib::Headers headers = {{ "Accept", "application/fhir+json" }};
};

#endif  // _FHIR_HPP_
