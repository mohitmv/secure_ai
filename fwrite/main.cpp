#include <stdio.h>

#include "fwrite.h"

#include <iostream>
#include <chrono>

using std::cout;
using std::endl;
using std::string;


inline int64_t GetEpochMicroSeconds() {
  using namespace std::chrono;  // NOLINT
  auto epoch_time = system_clock::now().time_since_epoch();
  return duration_cast<microseconds>(epoch_time).count();
}

void Test() {
  secure_ai::FILE* file1 = secure_ai::fopen("/tmp/mohit1");
  assert(secure_ai::fwrite("Mohit Saini", 1, 6, file1) == 6);
  secure_ai::fclose(file1);
}


template<typename T, typename FWrite, typename Fclose>
size_t BenchmarkTest(T* file, const FWrite* fwrite_func, const Fclose* fclose_func) {
  string a = "Mohit Saini";
  auto start = GetEpochMicroSeconds();
  for (int i = 0; i < 100000; i++) {
    fwrite_func(&a[0], 1, 6, file);
  }
  fclose_func(file);
  return GetEpochMicroSeconds() - start;
}

int main() {
  secure_ai::FILE* file1 = secure_ai::fopen("/tmp/mohit1");
  FILE* file2 = fopen("/tmp/mohit2", "w");
  secure_ai::FILE* file3 = secure_ai::fopen("/tmp/mohit3");

  auto time1 = BenchmarkTest(file1, &secure_ai::fwrite, &secure_ai::fclose);
  auto time2 = BenchmarkTest(file2, &fwrite, &fclose);
  auto time3 = BenchmarkTest(file3, &secure_ai::fwrite_unsafe, &secure_ai::fclose);

  cout << "Time taken by 'secure_ai::fwrite' = " << time1 << " microseconds" << endl;
  cout << "Time taken by 'fwrite' = " << time2 << " microseconds" << endl;
  cout << "Time taken by 'secure_ai::fwrite_unsafe' = " << time3 << " microseconds" << endl;

  return 0;
}

