
g++ -std=c++11 -O3 -c fwrite.cpp -o fwrite.o &&
g++ -std=c++11 -O3 main.cpp fwrite.o -o main &&
for i in {0..10}; do echo -e "\n\n----- Experiment "$i" --------\n" ;  ./main ; done
echo -e "\n\n"
