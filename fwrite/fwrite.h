#ifndef _FWRITE_HPP_
#define _FWRITE_HPP_


#include <pthread.h>
#include <stdio.h>
#include <string.h>

namespace secure_ai {

struct FILE {
  int file_descriptor;
  pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
  void* buffer = NULL;
  unsigned int buffer_size = 0;
  bool is_write_mode = true;
};

// Opens the file in write mode.
FILE* fopen(const char* file_path);

// thread-safe
size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE* stream);

// It's not thread safe.
size_t fwrite_unsafe(const void *ptr, size_t size, size_t nmemb, FILE* stream);

// 1. if buffer is storing some data, write it on disc.
// 2. close the file descriptor.
int fclose(FILE* stream);

// Same as fwrite but slightly optimized on fwrite calls which don't trigger
// disk-write. Such fwrite calls can work in parallel and copy their data in
// buffer at their respective allocated space/portion in buffer.
size_t fwrite_optimized(const void *ptr, size_t size, size_t nmemb, FILE *stream);

}  // namespace secure_ai


#endif  // _FWRITE_HPP_
