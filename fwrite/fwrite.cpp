#include "fwrite.h"

#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/stat.h>

#define BUFFER_CAPACITY 500000

namespace secure_ai {


// @to_write : number of bytes we need to write.
// @buffer_size : # of bytes present in buffer due to previous fwrite calls.
// Invariant : At the end of fwrite call, @buffer_size < BUFFER_CAPACITY.
//             It will never be equal to BUFFER_CAPACITY.
// if buffer was empty and @to_write >= BUFFER_CAPACITY -> write directly.
// else - we need to involve the buffer
//    if @buffer_size + @to_write >= BUFFER_CAPACITY
//        => buffer will overflow if we write everything in buffer. So fill
//           the buffer up to it's capacity, and then write buffer's data in
//           disc using 'write' call.
//        => if the number of remaining bytes >= BUFFER_CAPACITY
//             => directly write in the disc.
//           else
//             => add the data in buffer only.
//    else
//        => add the data in buffer only.

// known issues:
// 1. In the `if(written < BUFFER_CAPACITY) return 0;` part, there is a
// possibility of bug when 'write' was able to write partially but failed to
// write completly due to disc-size-overflow. In that case caller of fwrite
// will think zero bytes were written by fwrite call but actually
// non-zero bytes were written in disc. This case is difficult to handle
// without rollback functionality in underlying file_descriptor. In absense of
// rollback functionality, we need to do data relocation within buffer to 
// ensure it's consistent with what is already written in disc.
size_t fwrite_basic(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
  if (ptr == NULL ||
      stream == NULL ||
      size == 0 ||
      nmemb == 0 ||
      !stream->is_write_mode)
    return 0;
  int to_write = size * nmemb;
  int written;
  if (stream->buffer_size == 0 && to_write >= BUFFER_CAPACITY) {
    written = write(stream->file_descriptor, ptr, to_write);
    return (written == -1 ? 0 : written/size);
  }
  if (stream->buffer == NULL) {
    stream->buffer = malloc(BUFFER_CAPACITY);
  }
  if (stream->buffer_size + to_write >= BUFFER_CAPACITY) {
    int space = BUFFER_CAPACITY - stream->buffer_size;
    assert(space > 0 && stream->buffer_size > 0);
    memcpy((char*)stream->buffer + stream->buffer_size, ptr, space);
    written = write(stream->file_descriptor, stream->buffer, BUFFER_CAPACITY);
    if (written < BUFFER_CAPACITY) {
      return 0;
    }
    to_write -= space;
    ptr = (char*)ptr + space;
    stream->buffer_size = 0;
    if (to_write >= BUFFER_CAPACITY) {
      written = write(stream->file_descriptor, ptr, to_write);
      if (written == -1) {
        return 0;
      } else if (written < to_write) {
        return (space + written)/size;
      } else {
        return nmemb;
      }
    }
  }
  memcpy((char*)stream->buffer + stream->buffer_size, ptr, to_write);
  stream->buffer_size += to_write;
  assert(stream->buffer_size < BUFFER_CAPACITY);
  return nmemb;
}


int fwrite_validate(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
  if (ptr == NULL ||
      stream == NULL ||
      size == 0 ||
      nmemb == 0 ||
      stream->file_descriptor == -1 ||
      !stream->is_write_mode)
    return 0;
  return 1;
}


size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
  if (!fwrite_validate(ptr, size, nmemb, stream))
    return 0;
  pthread_mutex_lock(&stream->mutex);
  size_t output = fwrite_basic(ptr, size, nmemb, stream);
  pthread_mutex_unlock(&stream->mutex);
  return output;
}

size_t fwrite_unsafe(const void *ptr, size_t size, size_t nmemb, FILE* stream) {
  if (!fwrite_validate(ptr, size, nmemb, stream))
    return 0;
  return fwrite_basic(ptr, size, nmemb, stream);
}


// It's similar to fwrite. Only difference in 'fwrite_optimized' is due to placement of locks at appropriated places, instead of overall lock cover.


// Implementation Logic:
// Maintain two locks: 1. Simple lock, 2. ReadWrite lock.
// fwrite(...) {
//   simple_lock.lock();
//   check if this fwrite call cause disc write or not
//      ( i.e. check if `buffer_size + to_write >= BUFFER_CAPACITY`)
//   if (no need to write on disc) {
//      update the buffer_size to allocate the space in buffer for this fwrite
//      call.
//      read_write_lock.lock(READ);
//      simple_lock.unlock();
//      copy the data in buffer in the allocated space. Note that other fwrite
//      calls could be doing the same thing in parallel, but they are allocated a
//      different space in buffer. So it's safe.
//      read_write_lock.unlock(READ);
//   } else {
//      read_write_lock.lock(WRITE);
//      All the computations that involves the disc, should be done here.
//      read_write_lock.unlock(WRITE)
//      simple_lock.unlock()
//   }
size_t fwrite_optimized(const void *ptr, size_t size, size_t nmemb, FILE *stream);


FILE* fopen(const char* file_path) {
  FILE* file = new FILE();
  mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
  file->file_descriptor = open(file_path, O_WRONLY | O_CREAT | O_TRUNC, mode);
  return file;
}

int fclose(FILE* file) {
  if (file == NULL)
    return 1;
  int written;
  int output = 0;
  if (file->buffer != NULL) {
    if (file->buffer_size > 0) {
      written = write(file->file_descriptor, file->buffer, file->buffer_size);
      if (written == file->buffer_size) {
        output = 1;
      }
    }
    free(file->buffer);
  }
  output |= close(file->file_descriptor);
  free(file);
  return output;
}


}  // namespace secure_ai

