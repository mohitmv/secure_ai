#include "Python.h"
#include "httplib.h"
static PyObject *
spam_system(PyObject *self, PyObject *args);
static PyObject *
http_get(PyObject *self, PyObject *args);
char * http_get_data();

static PyObject *SpamError;

static PyMethodDef SpamMethods[] = {
  {"system",  spam_system, METH_VARARGS,
   "Execute a shell command."},
  {"get", http_get, METH_VARARGS, "Call a Rest API"},
  {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef spammodule = {
  PyModuleDef_HEAD_INIT,
  "spam",   /* name of module */
  "spam_doc", /* module documentation, may be NULL */
  -1,       /* size of per-interpreter state of the module,
         or -1 if the module keeps state in global variables. */
  SpamMethods
};




PyMODINIT_FUNC
PyInit_spam(void) {
  PyObject *m;
  m = PyModule_Create(&spammodule);
  if (m == NULL)
    return NULL;
  SpamError = PyErr_NewException("spam.error", NULL, NULL);
  Py_XINCREF(SpamError);
  PyModule_AddObject(m, "error", SpamError);
  return m;
}

static PyObject *
spam_system(PyObject *self, PyObject *args) {
  const char *command;
  int sts;
  if (!PyArg_ParseTuple(args, "s", &command))
    return NULL;
  sts = system(command);
  if (sts < 0) {
    PyErr_SetString(SpamError, "System command failed");
    return NULL;
  }
  return PyLong_FromLong(sts);
}

static PyObject * http_get(PyObject *self, PyObject *args) {
  httplib::Client cli("stu3.test.pyrohealth.net");
  httplib::Headers headers = {
    { "Accept", "application/fhir+json" }
  };
  auto res2 = cli.Get("/fhir/Patient/f772214d-0190-4e55-9d94-6c7498feee9f", headers);
  if (res2 && res2->status == 200) {
    PyObject *ret = Py_BuildValue("s", res2->body.c_str());
    return ret;
  }
  return NULL;
}

